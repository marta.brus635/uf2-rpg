using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class HealPatch : MonoBehaviour
{
    //[SerializeField]
    //Tilemap m_healPatch;

    [SerializeField]
    private PlayerScriptable m_player;

    private List<MyVeggieScriptable> equipo;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        equipo = m_player.equipo;

        foreach (MyVeggieScriptable veggie in equipo)
        {
            veggie.hpActual = veggie.hpMax;
        }
        Debug.Log("Equipo curado");
    }

}
