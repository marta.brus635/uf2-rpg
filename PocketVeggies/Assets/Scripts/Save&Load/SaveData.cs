using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct SaveGameData
{
    public string level;
    public Vector3 position;
    //public PlayerScriptable player;
}
