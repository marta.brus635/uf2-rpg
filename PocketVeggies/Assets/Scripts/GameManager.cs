using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;
using Scene = UnityEngine.SceneManagement.Scene;
using OdinSerializer;
using System.IO;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    PlayerScriptable m_Player;
    private static GameManager m_Instance;
    public static GameManager Instance
    {
        get { return m_Instance; }
    }

    private SaveGameData m_CurrentSavedData;
    private GameObject m_canvas;

    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        m_canvas = GameObject.Find("Canvas");
        m_canvas.SetActive(false);

        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(m_canvas);
        SceneManager.sceneLoaded += OnSceneLoaded;
        //InitValues();
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.P) && SceneManager.GetActiveScene().name == "World")
        {
            
            if (m_canvas.activeSelf) // is active????
                m_canvas.SetActive(false);

            else
                m_canvas.SetActive(true);

            Debug.Log("Canvas -> " + m_canvas.activeSelf);
        }
    }

    private void getCanvas()
    {
        if (m_canvas == null)
        {
            m_canvas = GameObject.Find("Canvas");
            Debug.Log("Canvas -> " + m_canvas.activeSelf);
            m_canvas.SetActive(false);
        }

    }
    //Es crida al carregar una escena
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        getCanvas();
       
        //if (scene.name == "GameScene")
        //    InitValues();
        //if (scene.name == "Battle")
        // PrepareBattle();
    }

    //private void InitValues(){}

    public void Battle()
    {
        SaveBeforeBattle();
        m_canvas.SetActive(false);
        SceneManager.LoadScene("TestBattleMarta");
    }

    // Evento y tal
    public void EndBattle(bool victoria)
    {
        if (victoria)
        {
            for (int i = 0; i < m_Player.equipo.Count; i++)
                m_Player.equipo[i].GanarExperiencia(50);
        }

        SceneManager.LoadScene("World");
        LoadGameAfterBattle();
        

    }

    public void OnChangeLevel()
    {
        if (SceneManager.GetActiveScene().name == "World")
            SceneManager.LoadScene("TestBattleMarta");
        else SceneManager.LoadScene("World");
    }


    //      ---     Save posPlayer      ---
    public void SaveBeforeBattle()
    {
        if(SceneManager.GetActiveScene().name != "SaveGameINIT")
        {
            SaveGameData saveGameData = new SaveGameData();


            PlayerController player = FindObjectOfType<PlayerController>();

            saveGameData.position = player.transform.position;
            byte[] serializedData = SerializationUtility.SerializeValue<SaveGameData>(saveGameData, DataFormat.JSON);
            string base64 = System.Convert.ToBase64String(serializedData);

            File.WriteAllText("playerposition.json", base64);
            Debug.Log("Desant el fitxer savegame.json");
            Debug.Log(saveGameData);
        }
    }

    public void LoadGameAfterBattle()
    {
        try
        {
            Debug.Log("Carregant el fitxer: playerposition.json");
            string newBase64 = File.ReadAllText("playerposition.json");
            byte[] serializedData = System.Convert.FromBase64String(newBase64);

            m_CurrentSavedData = SerializationUtility.DeserializeValue<SaveGameData>(serializedData, DataFormat.JSON);
            Debug.Log(m_CurrentSavedData);
            //subscribe to the scene loaded
            SceneManager.sceneLoaded += LoadGameSceneLoadedBattle;
            //change scene to the specified one
            //Debug.Log("Canviant a escena: " + m_CurrentSavedData.level);
            //SceneManager.LoadScene(m_CurrentSavedData.level);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }

    }

    private void LoadGameSceneLoadedBattle(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Escena carregada en mode LOAD : " + scene.name);
        //we are called because we are loading and the scene has been loaded
        //load values to the player
        FindObjectOfType<PlayerController>().Load(m_CurrentSavedData);
        //unsubscribe from the onloadscene
        SceneManager.sceneLoaded -= LoadGameSceneLoadedBattle;
    }

    //  ---     SaveGame    ---
    public void SaveGame()
    {
        //Al main menu no guardem
        if (SceneManager.GetActiveScene().name != "SaveGameINIT")
        {
            SaveGameData saveGameData = new SaveGameData();
            saveGameData.level = SceneManager.GetActiveScene().name;
            PlayerController player = FindObjectOfType<PlayerController>();
            //saveGameData.player.position = player.transform.position;
            //saveGameData.player.rotation = player.transform.rotation;
            saveGameData.position = player.transform.position;
            byte[] serializedData = SerializationUtility.SerializeValue<SaveGameData>(saveGameData, DataFormat.JSON);
            string base64 = System.Convert.ToBase64String(serializedData);
            //File.WriteAllBytes("savegame.json", serializedData);
            File.WriteAllText("savegame.json", base64);
            Debug.Log("Desant el fitxer savegame.json");
            Debug.Log(saveGameData);
        }
    }

    public void LoadGame()
    {
        try
        {
            Debug.Log("Carregant el fitxer: savegame.json");
            string newBase64 = File.ReadAllText("savegame.json");
            byte[] serializedData = System.Convert.FromBase64String(newBase64);

            m_CurrentSavedData = SerializationUtility.DeserializeValue<SaveGameData>(serializedData, DataFormat.JSON);
            Debug.Log(m_CurrentSavedData);
            //subscribe to the scene loaded
            SceneManager.sceneLoaded += LoadGameSceneLoaded;
            //change scene to the specified one
            Debug.Log("Canviant a escena: " + m_CurrentSavedData.level);
            SceneManager.LoadScene(m_CurrentSavedData.level);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }

    }

    private void LoadGameSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Escena carregada en mode LOAD : " + scene.name);
        //we are called because we are loading and the scene has been loaded
        //load values to the player
        FindObjectOfType<PlayerController>().Load(m_CurrentSavedData);
        //unsubscribe from the onloadscene
        SceneManager.sceneLoaded -= LoadGameSceneLoaded;
    }
}
