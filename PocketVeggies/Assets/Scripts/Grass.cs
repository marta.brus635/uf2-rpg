using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Tilemaps;
using static Unity.Burst.Intrinsics.X86;

public class Grass : MonoBehaviour
{
    [SerializeField]
    private Tilemap tileMap;
    private Vector3Int coordAct;
    private Vector3Int coordNova;

    [SerializeField]
    private int provabilidad;
    private int random;

    [SerializeField]
    private GameEvent onBattle;
    [SerializeField]
    private List<VeggieScriptable> VeggiesSalvatges;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        coordAct = tileMap.WorldToCell(collision.transform.position);

        gachapon();

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        coordNova = tileMap.WorldToCell(collision.transform.position);

        if (coordAct != coordNova)
            gachapon();

        coordAct = coordNova;
    }

    private void gachapon()
    {
        random = Random.Range(1, provabilidad);
        Debug.Log("GACHAPON: " + random);
        if (random == 1)
            onBattle.Raise();
    }

}
