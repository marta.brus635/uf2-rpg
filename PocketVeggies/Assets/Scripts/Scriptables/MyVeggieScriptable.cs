using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableMyVeggie", menuName = "Scriptables/ScriptableMyVeggie")]
public class MyVeggieScriptable : ScriptableObject
{
    public VeggieScriptable OriginalVeggie; 
    public string nom;

    public List<AttackScriptable> attacks;

    public int hpActual;
    public int hpMax;
    
    public int MineralActual;
    public int MineralMax;

    public int Exp;
    public int Nivel;

    public int Atk;
    public int Def;

    public Sprite Sprite;

    private void Awake()
    {
        Sprite = OriginalVeggie.Sprite;
    }

    public void GanarExperiencia(int valor)
    {
        Exp += valor;
        if(Exp >= 100)
        {
            PujarNivell(Exp / 100);
            Exp = Exp % 100;
        }
    }

    public void PujarNivell(int nivells)
    {
        Nivel += nivells;

        for (int i = 0; i < nivells; i++)
        {
            hpMax++;
            MineralMax++;

            Atk++;
            Def++;
        }
        hpActual = hpMax;
        MineralActual = MineralMax;
    }
}
