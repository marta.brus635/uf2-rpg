using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    [SerializeField]
    PlayerScriptable m_Player;
    private MyVeggieScriptable veggieLuchador;
    [SerializeField]
    GameEvent m_TornPlayer;
    [SerializeField]
    GameEvent m_TornRival;
    [SerializeField]
    GameEvent1Int m_Atacar;
    private bool isTornRival;
    [SerializeField]
    GameEvent m_CambioVeggie;
    [SerializeField]
    public GameEvent1Bool m_EndBattle;

    private static BattleManager m_Instance;
    public static BattleManager Instance
    {
        get { return m_Instance; }
    }
    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        SetTorn(false);
        veggieLuchador = m_Player.equipo[0];
    }

    private void SetTorn(bool tornRival)
    {
        isTornRival = tornRival;
        if (isTornRival)
        {
            m_TornRival.Raise();
        }
        else
        {
            m_TornPlayer.Raise();
        }
    }

    public void CanviTorn()
    {
        SetTorn(!isTornRival);
    }

    public void RebreTorn(int mal)
    {
        int dmgTotal = mal / m_Player.equipo[0].Def + 10;
        veggieLuchador.hpActual -= dmgTotal;

        if (veggieLuchador.hpActual <= 0)
        {
            Debug.Log("MyVeggieDed");
            veggieLuchador.hpActual = 0;

            // que salte al siguiente en el equipo
            veggieLuchador = nextVeggie(m_Player.equipo);
            if (veggieLuchador == null)
            {
                Debug.Log("NULL");
                EndBattle();
            }
            else
                m_CambioVeggie.Raise();
            
            
        }
        CanviTorn();
    }

    public int getDamage(int hp, int atk, int def)
    {
        Debug.Log("Damage: " + (atk / def + 10));
        return (atk / def + 10);

    }

    public void Atacar()
    {
        //Debug.Log("EntroAtacar");
        m_Atacar.Raise(30);
        CanviTorn();
    }

    // comprovar que hay mas veggies en el equipo
    private MyVeggieScriptable nextVeggie(List<MyVeggieScriptable> eq)
    {
        //  iterable pero no
        foreach (MyVeggieScriptable veggie in eq)
        {
            if (veggie.hpActual > 0)
                return veggie;
        }
        return null;
    }

    public void EndBattle()
    {
        m_EndBattle.Raise(false);
    }

    public void Pocion()
    {
        veggieLuchador.hpActual = veggieLuchador.hpMax;
        Debug.Log(veggieLuchador.nom + " curado! Y te ha salido gratis yujuuuuuuuuu (no tenemos inventario)");
        CanviTorn();
    }

    public void Captura()
    {
        Debug.Log("He usado una maceta para capturar el veggie pero aun no est� hecha la funci�n entonces pierdes turno inutilmente");
        CanviTorn();
    }
}
