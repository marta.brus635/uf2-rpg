using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - 1 int")]
public class GameEvent1Int : GameEvent<int> { }
