using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - 1 bool")]
public class GameEvent1Bool : GameEvent<bool> { }
