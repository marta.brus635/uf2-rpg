using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private PlayerScriptable playerScriptable;

    private Rigidbody2D rb;

    [SerializeField]
    private float vel;

    private Vector3 movement;

    void Start()
    {
        rb=this.GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            movement += transform.up;

        if (Input.GetKey(KeyCode.D))
            movement += transform.right;

        if (Input.GetKey(KeyCode.S))
            movement -= transform.up;

        if (Input.GetKey(KeyCode.A))
            movement -= transform.right;

        transform.position += movement.normalized * vel * Time.deltaTime;

    }

    public void Load(SaveGameData data)
    {
        Debug.Log("Player carregant informaci.");
        transform.position = data.position;

    }


}
