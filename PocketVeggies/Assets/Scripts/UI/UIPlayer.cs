using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIPlayer : MonoBehaviour
{
    [SerializeField]
    private PlayerScriptable m_Player;
    [SerializeField]
    private Image m_Sprite;
    [SerializeField]
    private TextMeshProUGUI m_Nom;
    [SerializeField]
    private TextMeshProUGUI m_Exp;
    [SerializeField]
    private TextMeshProUGUI m_vida;
    [SerializeField]
    private TextMeshProUGUI m_Minerals;
    [SerializeField]
    private TextMeshProUGUI m_Nivell;
    //Para saber cual veggie del array tengo que cargar la info
    private int m_VeggieLuchador;
    /*
    [SerializeField]
    private TextMeshProUGUI m_AttackNom;
    [SerializeField]
    private TextMeshProUGUI m_AttackDmg;
    [SerializeField]
    private TextMeshProUGUI m_AttackCost;
    */

    private void Awake()
    {
        m_VeggieLuchador = 0;
        LoadInfo();
    }

    public void LoadInfo()
    {
        m_Sprite.sprite = m_Player.equipo[m_VeggieLuchador].Sprite;
        m_Nom.text = m_Player.equipo[m_VeggieLuchador].nom;
        m_Exp.text = "Exp: " + m_Player.equipo[m_VeggieLuchador].Exp;
        m_vida.text = "HP: " + m_Player.equipo[m_VeggieLuchador].hpActual;
        m_Minerals.text = "Minerals: " + m_Player.equipo[m_VeggieLuchador].MineralActual;
        m_Nivell.text = "Lvl: " + m_Player.equipo[m_VeggieLuchador].Nivel;

        //m_AttackNom.text = m_Player.equipo[0].attacks[0].ToString();
    }

    public void CambioVeggie()
    {
        m_VeggieLuchador++;
        LoadInfo();
    }

}
