using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.VisualScripting;
using UnityEngine.UI;

public class UIRival : MonoBehaviour
{
    [SerializeField]
    private MyVeggieScriptable m_Conejollo;
    [SerializeField]
    private TextMeshProUGUI m_Nom;
    [SerializeField]
    private Image m_Sprite;
    [SerializeField]
    private TextMeshProUGUI m_Minerals;
    [SerializeField]
    private TextMeshProUGUI m_Nivel;
    [SerializeField]
    private TextMeshProUGUI m_HP;
    [SerializeField]
    private GameEvent1Int m_TornAcabat;


    private void Awake()
    {
        LoadInfo();
    }

    public void LoadInfo()
    {
        m_Sprite.sprite = m_Conejollo.Sprite;
        m_Nom.text = m_Conejollo.nom;
        m_Minerals.text = "Minerals: " + m_Conejollo.MineralActual;
        m_Nivel.text = "Lvl: " + m_Conejollo.Nivel;
        m_HP.text = "HP: " + m_Conejollo.hpActual;
    }

    public void MeToca()
    {
        //Debug.Log("Rival pensando su ataque...");
        StartCoroutine(Atacar());
    }

    IEnumerator Atacar()
    {
        //TODO
        yield return new WaitForSeconds(3);
        /*
         * Acci� enemic 
         */
        m_TornAcabat.Raise(10); //<- Cambiar por da�o real
        StopAllCoroutines();
    }

    public void RebreTorn(int mal)
    {
        m_Conejollo.hpActual -= mal;
        //Debug.Log(m_Conejollo.hpActual);

        if(m_Conejollo.hpActual <= 0)
        {
            BattleManager.Instance.m_EndBattle.Raise(true);
        }
    }

}
